<?php

namespace WinelCategoryContentCopy\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CopySubscriber implements SubscriberInterface
{
    private $container;
    private $shopId;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    private function getPluginPath()
    {
        return $this->container->getParameter('winel_category_content_copy.plugin_dir');
    }
    private function getPluginId()
    {
        $cronjob  = Shopware()->Db()->fetchAll(
            'SELECT id
            FROM s_core_plugins scp
            WHERE scp.name = ? ',['WinelCategoryContentCopy'] ) ;
        return $cronjob[0]['id'];
    }

    public static function getSubscribedEvents()
    {
        return [
            'Shopware_CronJob_WinelCategoryContentCopyCron' => 'WinelCategoryContentCopyRun'
        ];
    }

    public function WinelCategoryContentCopyRun()
    {
        $shops=$this->getShops();

        foreach ($shops as $shop){
            $shopId=$shop['id'];
            $this->shopId=$shopId;
            $shopBaseCat=$shop['category_id'];
            $shopConfig=$this->getShopPluginConfig($shopId);
            if($shopConfig['copy']){
                $masterShopName=$shopConfig['master'];
                $masterShop=$this->getShopByName($masterShopName);
                $baseFieldsArray=$shopConfig['baseFields'];
                $attributeFieldsArray=$shopConfig['attributeFields'];
                $masterShopCategory=$masterShop[0]['category_id'];
                $this->copyCategories($shopBaseCat,$masterShopCategory,$baseFieldsArray,$attributeFieldsArray);
            }
        }
    }

    public function copyCategories($shopBaseCat, $masterShopCategory, $baseFieldsArray, $attributeFieldsArray){
        $categories=$this->getShopCategories($shopBaseCat);
        $translation=false; // Umstellmöglichkeit, falls die nächste SW Version bei Kategorien mit Translations arbeitet
        foreach($categories as $category) {
            $correspondingCat='';
            $categoryId = $category['id'];
            $categoryAttributes=$this->getCategoryAttributes($categoryId);
            $base_category=$categoryAttributes['base_category'];
            if($categoryId==$base_category){
                $correspondingCat=$categoryId;
            }elseif( $base_category!==NULL && trim($base_category)!==''  ){
                $correspondingCat= $this->getCorrespondingCategory($masterShopCategory,$base_category);
                if(count($correspondingCat)>0) {
                    $correspondingCat=$correspondingCat['categoryID'];
                }
            }
            if($correspondingCat !==''){
                if($translation) {
                    $masterShopId = $this->getMasterShopId($this->shopId);
                    $masterText = $this->getTranslations($correspondingCat, $masterShopId);
                    if ($masterText !== '') {
                        $aktShopTextArray = $this->getTranslations($correspondingCat, $this->shopId);
                        $newTextArray = $this->buildNewTranslationTextArray($masterText, $aktShopTextArray, $baseFieldsArray, $attributeFieldsArray);
                        if ($aktShopTextArray === '') {
                            $this->saveTranslations($this->shopId, $categoryId, $newTextArray);
                        } else {
                            $this->updateTranslations($this->shopId, $categoryId, $newTextArray);
                        }
                    }
                }else{
                    $masterCategoryAttributes=$this->getCategoryAttributes($correspondingCat);
                    $newTextArray = $this->buildNewTextArray($masterCategoryAttributes,  $baseFieldsArray, $attributeFieldsArray);
                    $this->updateCat($categoryId,$newTextArray['Base'],'s_categories');
                    $this->updateCat($categoryId,$newTextArray['Attributes'], 's_categories_attributes');
                }

            }
        }
    }
    public function buildNewTextArray($masterCategoryAttributes,$baseFieldsArray,$attributeFieldsArray){
        $aktShopTextArray=[];
        foreach($baseFieldsArray as $baseField){
            $aktShopTextArray['Base'][$baseField]=$masterCategoryAttributes[$baseField];
            if(!isset($aktShopTextArray['Base'][$baseField])){
                $aktShopTextArray['Base'][$baseField]='';
            }
        }
        foreach($attributeFieldsArray as $attributeField){
            $aktShopTextArray['Attributes'][$attributeField]=$masterCategoryAttributes[$attributeField];
            if(!isset($aktShopTextArray['Attributes'][$attributeField])){
                $aktShopTextArray['Attributes'][$attributeField]='';
            }
        }
        return $aktShopTextArray;
    }

    public function buildNewTranslationTextArray($masterText,$aktShopTextArray,$baseFieldsArray,$attributeFieldsArray){

        if($aktShopTextArray===''){
            $aktShopTextArray=[];
        }
        foreach($baseFieldsArray as $baseField){
            if(!isset($masterText[$baseField])){
                $aktShopTextArray[$baseField]='';
            }else{
                $aktShopTextArray[$baseField]=$masterText[$baseField];
            }
        }
        foreach($attributeFieldsArray as $attributeField){
            $attributeFieldName='__attribute_'.$attributeField;
            if(!isset($masterText[$attributeFieldName])){
                $aktShopTextArray[$attributeFieldName]='';
            }else{
                $aktShopTextArray[$attributeFieldName]=$masterText[$attributeFieldName];
            }
        }
        unset( $aktShopTextArray['name']);
        return $aktShopTextArray;
    }

    public function getMasterShopId($shopId)
    {
        $shop  = Shopware()->Db()->fetchRow(
            'SELECT *
            FROM s_core_shops scs
            WHERE scs.id = ? ',[$shopId] ) ;
        if($shop['main_id']!=='' && $shop['main_id']!==NULL){
            return $shop['main_id'];
        }
        return $this->shopId;
    }

    public function getCategoryAttributes($categoryId){
        $attributes  = Shopware()->Db()->fetchRow(
            'SELECT *
            FROM s_categories sc left join s_categories_attributes sca on sc.id=sca.categoryID
            WHERE sc.id = ? ',[$categoryId] ) ;
        return $attributes;
    }

    public function updateCat($categoryId,$newTextArray,$table){

        if(count($newTextArray)>0){
            $x=0;
            $values=[];
            $sql='update '.$table.' set ';
            foreach ($newTextArray as $name => $value){
                if($name!='description'){
                    if($value=='') {
                        $value=NULL;
                    }
                    if($x>0){
                        $sql.= ', ';
                    }
                    $sql.= $name.'=? ';
                    $x++;
                    $values[]=$value;
                }
            }
            if($table==='s_categories'){
                $sql.=' where id= ?';
            }else{
                $sql.=' where categoryID= ?';
            }
            $values[]=$categoryId;

            if($x>0){
                  $this->container->get('dbal_connection')->executeQuery($sql, $values);
            }
        }

    }

    public function getShopByName($shopName){
        $shop  = Shopware()->Db()->fetchAll(
            'SELECT id, category_id
            FROM s_core_shops sc
            WHERE sc.name = ? ',[$shopName] ) ;
        return $shop;
    }

    public function getShops(){
        $shops  = Shopware()->Db()->fetchAll(
            'SELECT id,name, category_id
            FROM s_core_shops sc
            WHERE sc.active = ? ',[1] ) ;
        return $shops;

    }

    public function getCorrespondingCategory($baseCat,$catId){
        $categories  = Shopware()->Db()->fetchRow(
            'SELECT * 
            FROM s_categories sc left join s_categories_attributes sca on sc.id=sca.categoryID where sc.path like (?) and sca.base_category=?
            ',['%|'.$baseCat.'|%', $catId] ) ;
        return $categories;

    }

    public function getTranslations($categoryId,$shopId){

        $translation  = Shopware()->Db()->fetchRow(
            'SELECT objectdata
            FROM s_core_translations sct
            WHERE sct.objecttype = ? and sct.objectkey=? and sct.objectlanguage=? ',['category',$categoryId,$shopId] ) ;

        echo   'SELECT objectdata
            FROM s_core_translations sct
            WHERE sct.objecttype = ? and sct.objectkey=? and sct.objectlanguage=? '. " \n\r";
        print_r(['category',$categoryId,$shopId]);
        $translationData=$translation['objectdata'];
        if($translationData ==NULL || $translationData==''){
            return '';
        }else{
            return unserialize($translationData);
        }
    }

    public function getBaseCategoryText($categoryId){
        $categoryText  = Shopware()->Db()->fetchRow(
            'SELECT * 
            FROM s_categories sc left join s_categories_attributes sca on sc.id=sca.categoryId where id =?
            ',[$categoryId] ) ;
        return $categoryText;
    }

    public function saveTranslations($shopId,$categoryId,$shopText){
        $serializedText=serialize($shopText);

        $this->container->get('dbal_connection')->executeQuery('insert into s_core_translations (objecttype,objectkey,objectlanguage,objectdata) Values (?,?,?,?)', [
            'category', $categoryId, $shopId, $serializedText
        ]);

    }
    public function updateTranslations($shopId,$categoryId,$shopText){
        $serializedText=serialize($shopText);

        $this->container->get('dbal_connection')->executeQuery('update s_core_translations set objectdata =? where objecttype=? and objectkey=? and objectlanguage=? ', [
            $serializedText,'category', $categoryId, $shopId
        ]);

    }





    public function getShopCategories($baseCat){
       $categories  = Shopware()->Db()->fetchAll(
            'SELECT * 
            FROM s_categories sc where path like (?)
            ',['%|'.$baseCat.'|%'] ) ;
        return $categories;

    }

    public function getShopPluginConfig($shopId){

        $pluginId=$this->getPluginId();
        $formId=$this->getFormId($pluginId);
        $formElements=$this->getFormElements($formId);
        $shopConfig=$this->getFormElementsValues($formElements,$shopId);
        return $shopConfig;

    }

    public function getFormId($pluginId){
        $forms  = Shopware()->Db()->fetchAll(
            'SELECT id,name 
            FROM s_core_config_forms ccf
            WHERE ccf.plugin_id = ? ',[$pluginId] ) ;
        return $forms[0]['id'];

    }

    public function getFormElements($formId){
        $formelEments  = Shopware()->Db()->fetchAll(
            'SELECT id,name 
            FROM s_core_config_elements cce
            WHERE cce.form_id = ? ',[$formId] ) ;
        return $formelEments;

    }

    public function getFormElementsValues($formElements,$shopId){
        $valueArray=[];
        foreach($formElements as $formElement){
            $elementId=$formElement['id'];
           $serializedValue=$this->getSerializedFormElementsValues($elementId,$shopId);
            $valueArray[$formElement['name']]=unserialize($serializedValue);

        }
        return $valueArray;

    }
    public function getSerializedFormElementsValues($elementId,$shopId){
        $value  = Shopware()->Db()->fetchAll(
            'SELECT id,value 
            FROM s_core_config_values ccv
            WHERE ccv.element_id = ?  and shop_id=?',[$elementId,$shopId] ) ;
        return $value[0]['value'];

    }


}