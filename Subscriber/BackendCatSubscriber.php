<?php

namespace WinelCategoryContentCopy\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BackendCatSubscriber implements SubscriberInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    private function getPluginPath()
    {
        return $this->container->getParameter('winel_category_content_copy.plugin_dir');
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_CategoryContentCopyStore' => 'onCategoryContentCopyStore'
        ];
    }

    public function onCategoryContentCopyStore()
    {
        return $this->getPluginPath() . '/Controllers/Backend/CategoryContentCopyStore.php';
    }


}