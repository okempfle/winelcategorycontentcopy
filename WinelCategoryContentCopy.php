<?php

namespace WinelCategoryContentCopy;

use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class WinelCategoryContentCopy extends Plugin
{
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('winel_category_content_copy.plugin_dir', $this->getPath());
        $container->setParameter('winel_category_content_copy.plugin_name', $this->getName());
        parent::build($container);
    }
}